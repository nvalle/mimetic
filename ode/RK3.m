classdef RK3 < RKM
%Rnge-Kutta 3rd order time integrator
%solves equations of the type dy/dt = f(y)
properties
end
methods
    function i = RK3(eq,varargin) %NOTE: we don't really need y, wee keep same signature as AB2
        i@RKM(3,eq);
        i.A(2,1) = 1;
        i.A(3,1) = 1/4;
        i.A(3,2) = 1/4;
        i.c      = [0;   1/2;   1   ];
        i.b      = [1/6; 1/6;   2/3 ];
    end
end
end

