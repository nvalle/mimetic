classdef RK2 < RKM
%Adams-Bashforth 2nd order time integrator
%solves equations of the type dy/dt = f(y)
properties
end
methods
    function i = RK2(eq,varargin) %NOTE: we don't really need y, wee keep same signature as AB2
        i@RKM(2,eq);
        i.A(2,1) = 1;
        i.b      = [1/2; 1/2];
    end
end
end

