classdef EU < integrator
%Euler 1st order time integrator
%solves equations of the type dy/dt = f(y)
properties
    R               %present residual
end
methods
    function i = EU(eq,varargin)
        i@integrator(eq);
    end
    function [r,u] = step(i,varargin)
        i.R  = i.f(varargin{:});
        u    = varargin{1} + i.dt*i.R;
        r    = max(i.R);
    end
end
end

