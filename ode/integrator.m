classdef integrator< solver
%Solve y'=f(y,t) equations. Captures the eq. pointer when constructed.
properties
    f               %function pointer
    dt   = 1e-4     %time step (default value)
    tmax = 1e6      %max independent variable
end
methods
    [r,y] = step(i,varargin)
    function i = integrator(f)
        i.f = f;
    end
    function setdt(i,dt)
        i.dt = dt;
    end
    function [r,y] = solve(i,varargin)
        s = 0;
        t = 0;
        r = 1.1*i.rmin;
        y = varargin{1};
        while (max(r) < i.rmax && s < i.smax && max(r)>i.rmin)
            [r,y]   = i.step(varargin{:});
            varargin{1} = y;
            s     = s+1;
            t     = t + i.dt;
        end
    end
end
end

