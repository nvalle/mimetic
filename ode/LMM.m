classdef LMM < integrator
%Solve y'=f(y,t) equations via Linear Multistep Method
%General formulation is ai*yi = bi*f(yi,ti)
properties
    a               %variable coefficients 
    b               %function coefficients
    Rp              %previous residuals
end
methods
    function l = LMM(eq,varargin)
        l@integrator(eq);
        l.Rp = l.f(varargin{:});
    end
end
end

