classdef AB2 < LMM
%Adams-Bashforth 2nd order time integrator
%solves equations of the type dy/dt = f(y)
properties
    R               %present residual
end
methods
    function i = AB2(eq,varargin)
        i@LMM(eq,varargin{:});
    end
    function [r,u] = step(i,varargin)
        i.R     = i.f(varargin{:});
        u       = varargin{1};
        u       = varargin{1} + i.dt*(1.5*i.R-0.5*i.Rp);
        i.Rp    = i.R;
        r       = norm(u-varargin{1},Inf)/i.dt;
    end
end
end

