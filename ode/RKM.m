classdef RKM < integrator
%Runge-Kutta family of integrators
%solves equations of the type dy/dt = f(y)
properties
    ns      %number of stages
    A       %Runge-Kuta matrix (explicit is lower triangular)
    b       %weight
    c       %node
    R       %residual
end
methods
    function i = RKM(ns,eq,y)
        i@integrator(eq);
        i.ns= ns;
        i.A = sparse(ns,ns);
        i.b = zeros(ns,1);
        i.c = zeros(ns,1);
    end
    
    function [r,y] = step(r,varargin)
        r.R = sparse(size(varargin{1},1),r.ns);      %initialize all steps
        y0  = varargin{1};
        for i=1:r.ns
            ys      = varargin;
            ys{1}   = ys{1}+r.dt*r.R*r.A(i,:)';
            r.R(:,i)= r.f(ys{:});
        end
        y   = varargin{1} + r.dt*r.R*r.b;
        r   = max(abs(r.f(y,varargin{[2:end]})));
    end
end
end

