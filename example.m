addpath(genpath('~/lib/mimetic'))   %load package

M = collocated(2,'square.mat');     %generate manifold

Re = 100;                           %state Reynolds

cf0 = Newtonian(M, Re);             %consititutive equations fluid 0

mf0 = NavierStokes(M,cf0);          %momentum equation fluid 0

[u,P] = mf0.init();                 %initialize variables