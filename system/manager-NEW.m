classdef manager < handle
    %SOLVER base class. TO BE IMPLEMENTED 
properties
    s    = 0        %present step
    smax = 1        %max number of steps
    csi  = 1        %control steps interval
    rmin = 1e-09    %convergence value
    rmax = 1e+09    %divergence value
    r    = 0        %residual
    t    = 0        %time
    dt   = 1e-6;    %time step
    tmax = 32      %max time
    nmodels         %number of models
    models          %models list
    Tcum            %cumulated total quantities
    Ecum            %cumulated Energy
    vcum            %cumulated variables
    ofile           %output file
    odir            %output directory
    mdir            %mesh directory
end

methods
    function m = manager(casename,mdir,varargin)
        m.nmodels   = size(varargin,2);             %number of models
        m.models    = varargin;                     %models list
        m.smax      = ceil(m.tmax)/m.dt;            %max number of steps
        m.setdt(m.autodt);                          %set dt for all models
        m.csi       = (m.smax-mod(m.smax,40))/40;   %control steps interval
        m.odir      = casename;
        mkdir(casename);
        ofilename   = strcat(m.odir,'/out.csv');
        m.ofile     = fopen(ofilename,'w');     %output file stream
        fprintf(m.ofile,'1-it;2-t;3-r0;4-r1;5-E0;6-E1;7-Et;8-px;9-py;10-th;11-dEpSdt;12-dEpndt;13-dEpdt;14-dEkkdt;15-dEkpdt;16-dEkdt;17-dEtdt;18-dmdt\n');
        m.mdir      = mdir;
    end
    function [rout,Eout,vout,Tout] = step(m,vin)
%         vout = vin;
        Eout = 0;
        output = cell(1,2);
        Tout = {};
        vout = {};                 %THIS NEEDS TO BE ADJUSTED IF
                                                %WE CHANGE THE NUMBER OF
                                                %MODELS
        for i=1:1:m.nmodels
            [rout(i),Eout(i),Tout{i},output{:}] = m.models{i}.step(vin{:});
            vout = {vout{:}, output{:}};
        end
    end
    function [vout,Eout,Tout] = solve(m,vin)         %solve coupled problem
        m.vcum = {};          %cummulated variables
        m.Ecum = [];          %cummulated energy
        m.Tcum = [];          %cummulated total quantities
        while 1
            [m.r,Eout,vout,Tout] = m.step(vin);
            if mod(m.s,m.csi)==0
                m.Ecum = [m.Ecum;[m.s,Eout]];
                m.vcum = [m.vcum;vout];
                m.Tcum = [m.Tcum;[m.s,Tout{:}]];
                m.control(vout,Eout,Tout);
            end
            m.s = m.s + 1;
            m.t = m.t + m.dt;
%             if (m.tmax/2 < m.t && m.t < m.tmax/2 + m.dt)
%                 vin{1} = -vin{1}; %invert rotation
%             end
            if ((norm(m.r,Inf) < m.rmin || norm(m.r,Inf) > m.rmax ...
                || m.s >= m.smax || m.t >= m.tmax))
                m.control(vout,Eout,Tout);
%                 m.plot(vout);
                break
            end
            vin = vout;
        end
        fclose(m.ofile);
    end
    function control(m,vout,Eout,Tout)
        IC = m.models{2}.IC;
        NS = m.models{1}.eq;
        u  = vout{1};
        p  = vout{2};
        th = vout{3};
        nf = vout{4};
        kf = IC.curvature(u,th);
        U  = spdiags(u,0,m.models{1}.M.T.ne,m.models{1}.M.T.ne);
        
        dEpSdt = NS.ceq.st*(IC.M.GRAD*IC.ADV(th,u))'*IC.M.T.Vs*nf;
%         dEpSdt = (LS.M.GRAD*(LS.Correction.R(th,nf,u)+LS.R(th,u)))'*LS.M.T.Vs*nf;
        dEpndt = 0;
        dEpdt  = dEpSdt + dEpndt;
        
        %----------TEST
%         uf = u;
%         uf(abs(uf)<1e-16) = 1;
%         Uf = spdiags(uf                     ,0,PF.M.T.ne,PF.M.T.ne);
%         N  = spdiags(nf                     ,0,LS.M.T.ne,LS.M.T.ne); 
%         M  = spdiags(N*LS.M.GRAD*th         ,0,LS.M.T.ne,LS.M.T.ne);
%         R  = spdiags(IC.M.T.sCF*((1-th).*th),0,IC.M.T.ne,IC.M.T.ne);
%         E  = 1/2;
        
%         Rc = spdiags(1-th                   ,0,LS.M.T.nc,LS.M.T.nc);

%         S  = LS.M.T.sCF - LS.HR.F(uf,th) + Uf\(N*R*LS.M.T.sCF - E*LS.M.GRAD);
        
%         S  = LS.M.T.sCF - LS.HR.F(uf,th) + Uf\(R*(M\LS.M.GRAD) - E*LS.M.GRAD);
        
%         isequal(LS.Correction.R(th,nf,u),-LS.M.DIV*(R*LS.M.T.Dx-E)*LS.M.GRAD*th)
%         isequal(PF.Correction.R(th,nf,u),-PF.M.DIV*U*R*PF.M.T.Dx*PF.M.GRAD*th+PF.M.DIV*U*E*PF.M.GRAD*th)
%         isequal(PF.R(th,u),-PF.M.DIV*U*(PF.M.T.sCF + PF.HR.F(uf,th))*th)
        
%         (PF.M.GRAD*PF.Correction.R(th,nf,u))'*PF.M.T.Vs*nf
        %--------------
%         dEkkdt= u'*NS.M.T.Vs*((IC.M.GRAD*th).*kf);
        dEkkdt= u'*NS.M.T.Vs*NS.CSF(u,th);
        dEkpdt= u'*NS.M.T.Vs*NS.M.GRAD*p;
        dEkcdt= u'*NS.M.T.Vs*NS.CONV(u,u);
        dEkgdt= u'*NS.M.T.Vs*NS.g;
        
        dEkdt = dEkkdt + dEkpdt;
        dEtdt = (dEkdt+dEpSdt);
        
        dpdt  = kf'*IC.M.T.Vs*IC.M.GRAD*th;             %Momentum imbalance
        dmdt  = ones(size(th))'*IC.M.T.Vc*IC.ADV(th,u); %Mass imabalance
        
        fprintf(m.ofile,'%05g;%3.9f;%10.9e;%10.9e;%10.9e;%10.9e;%10.9e;%10.9e;%10.9e;%+10.9e;%+10.9e;%+10.9e;%+10.9e;%+10.9e;%+10.9e;%+10.9e;%+10.9e;%+10.9e\n',...
                [m.s,m.t,m.r,Eout,norm(Eout,1),Tout{:,1},Tout{:,2},dEpSdt,dEpndt,dEpdt,dEkkdt,dEkpdt,dEkdt,dEtdt,dpdt]);
                
        disp(['it = ',num2str(m.s,'%05g'),...
              '     t = ',num2str(m.t,'%3.3f'),...
              '     r = ',num2str(m.r,'%10.3e'),...
              '     E = ',num2str(Eout,'%10.3e'),...
              '     Et= ',num2str(norm(Eout,1),'%10.3e')])
        disp(['     p = ',num2str(Tout{1},'%10.3e %10.3e'),...
              '     th= ',num2str(Tout{2},'%10.3e')])
        disp(['     dEpSdt    = ',num2str(dEpSdt,'%+10.3e'),...
              '     dEpndt    = ',num2str(dEpndt,'%+10.3e'),...
              '     dEpdt     = ',num2str(dEpdt,'%+10.3e'),...
              '   | dE(p+k)dt = ',num2str(dEkkdt+dEpdt,'%+10.3e')])
        disp(['     dEkkdt    = ',num2str(dEkkdt,'%+10.3e'),...
              '     dEkcdt    = ',num2str(dEkcdt,'%+10.3e'),...
              '     dEkpdt    = ',num2str(dEkpdt,'%+10.3e'),...
              '   | dEtdt     = ',num2str(dEtdt,'%+10.3e')])

        m.plot(vout);
        meshname = strcat(m.mdir,'/mesh.vtk');
        vc = reshape(NS.M.T.vFC*vout{1},NS.M.T.nn,NS.M.T.d);
        savevectorfield(vc,strcat(m.odir,'/u-',num2str(m.s),'.vtk'),meshname);
        savefield(vout{2},strcat(m.odir,'/P-',num2str(m.s),'.vtk'),meshname);
        savefield(vout{3},strcat(m.odir,'/th-',num2str(m.s),'.vtk'),meshname);
    end
    function plot(m,vin)
        npr = 2;         %number of plot rows
        npc = 2;         %number of plot columns
        nx = sqrt(m.models{1}.M.T.nn);
        ny = nx;
        x = reshape(m.models{1}.M.T.xn(:,1),nx,ny);
        y = reshape(m.models{1}.M.T.xn(:,2),nx,ny);      
        figure(1);
        subplot(npr,npc,1);
%         hold off;
%         scatter3(m.models{1}.M.T.xn(:,1),m.models{1}.M.T.xn(:,2),vin{2});
%         view(0,90);
%         hold on;
        v = m.models{1}.M.T.N'*vin{1};
        v = reshape(v,m.models{1}.M.T.ne,m.models{1}.M.T.d);
        z = reshape(vin{3},nx,ny);
        quiver(m.models{1}.M.T.xe(:,1),m.models{1}.M.T.xe(:,2),v(:,1),v(:,2))
        axis equal;
%         hold off;
        subplot(npr,npc,2);
        semilogy(m.Tcum(:,1),abs([vecnorm(m.Tcum(:,[2 3])')',m.Tcum(:,4)]));
%         figure(2);
        subplot(npr,npc,3);
        hold off;
%         F = -vin{4}.*(m.models{1}.M.GRAD+(m.models{1}.M.T.Dx\m.models{1}.M.T.DF*m.models{1}.M.T.Tu'))*vin{3};
        F = vin{4}.*(m.models{1}.M.GRAD*vin{3});
%         scatter3(m.models{1}.M.T.xe(:,1),m.models{1}.M.T.xe(:,2),F);
        F = m.models{1}.M.T.N'*F;
        F = reshape(F,m.models{1}.M.T.ne,m.models{1}.M.T.d);
        quiver(m.models{1}.M.T.xe(:,1),m.models{1}.M.T.xe(:,2),F(:,1),F(:,2))
%         n = m.models{2}.IC.normal(vin{3});
%         quiver(m.models{1}.M.T.xn(:,1),m.models{1}.M.T.xn(:,2),n(:,1),n(:,2))
        hold on;
%         contour(x,y,z,[0.05,0.5,0.95])
        contour(x,y,z,5)
%         scatter3(m.models{1}.M.T.xn(:,1),m.models{1}.M.T.xn(:,2),vin{3});
        vc = m.models{1}.M.T.vFC*vin{1};
        uc = reshape(vc([1:m.models{1}.M.T.nn]),nx,ny);
        vc = reshape(vc([m.models{1}.M.T.nn+1:end]),nx,ny);
        startx = m.models{1}.M.T.xn([nx/4:3/4*nx],1);
        starty = m.models{1}.M.T.xn([nx/4:3/4*nx],2);
        quiver(x,y,uc,vc);
%         streamline(x',y',uc,vc,startx',starty',[1,10e3]);
        hold off;
%         axis equal;
%         figure(3);
        subplot(npr,npc,4);
        plot(m.Ecum(:,1),[m.Ecum(:,2),m.Ecum(:,3),m.Ecum(:,2)+m.Ecum(:,3)]);
        legend({'Ek','Ep','Em'},'Location','northwest');
        legend('boxoff')
         %----------TEST
%         figure(4);
%         scatter3(m.models{1}.M.T.xn(:,1),m.models{1}.M.T.xn(:,2),vin{3});   
    end
    function dt = autodt(m)
        dt = m.models{1}.maxdt;
        for i=(2:m.nmodels)
            if m.models{i}.maxdt < dt 
                dt = m.models{i}.maxdt;
            end
        end
    end
    function setdt(m,dt)
        m.dt = dt;
        m.smax      = ceil(m.tmax)/m.dt;        %max number of steps
        for i=1:1:m.nmodels
            m.models{i}.setdt(m.dt);
        end
    end    
end
end

