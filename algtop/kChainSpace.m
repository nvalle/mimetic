classdef kChainSpace < handle
    %ChainSpace produces a vector space out of a cell space
    %   Every k-chain is defined by the k-cell the chain is attached to and
    %   the n-tuple of values which correspond with every k-cell. A default
    %   orientation is required for this vector basis, which actually
    %   correspond with the default orientation of every k-cell. Indeed,
    %   by assigning an orientation to a cell space we are already defining
    %   a vector basis and so promoting the cell space to a chain space.
    %   In turn, every k-cell is defined by its union of (k-1) boundaries.
    %   All chells and its links with boudnaries can be stacked into an
    %   incidence matrix.
    %   In addition, every set of k-cells is equipped with the trace
    %   operator, Tr : k-Cell -> d(k-Cell), which extracts the subset of
    %   boundary cells out of the whole set of k-Cells.
    properties
        k               %k-chain dimension
        nchains         %number of chains
        nbndary         %number of boundary columns
        tbndary         %boundary tags
        Tr              %Trace matrix
    end
    methods
        function obj = kChainSpace(k, nchains, bchain, btag)
            obj.k       = k;
            obj.nchains = nchains;
            obj.nbndary = numel(bchain);
            obj.tbndary = btag;
            trace_idx   = 1:obj.nbndary;
            obj.Tr      = sparse(trace_idx, bchain, 1, obj.nbndary, nchains);
%             %number of boundary segments
%             nbs = numel(varargin)
%             for b = 1:nbs
%                 %number of boundary chains
%                 nbc                 = size(varargin{b},1);
%                 obj.n_boundary      = obj.n_boundary + nbc;
% %                 obj.Tr{b} = sparse(1:nbc, varargin{b}, 1, nbc, nchains);
%             end
        end
    end
end

