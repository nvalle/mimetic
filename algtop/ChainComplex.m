classdef ChainComplex < handle
    %ChainComplex ensables a chain complex
    %   A chain complex is defined by its kChainSpaces (C) and its boundary
    %   operators (d), where d : k-ChainSpace -> (k-1)-ChainSpace is a linear
    %   map and thus represented as a (sparse) matrix.
    properties
        n                   % embedding dimension of the chain complex
        C                   % set of all kChainSpaces
        d                   % set of boundary operators
        hedge               % healing edges
    end
    methods
        function this = ChainComplex(varargin)
            n           = varargin{1};
            this.n      = n;
            if nargin > 1
                meshname = varargin{2};
                %import topology
                load(meshname);
                [nt, crow, bcol, dorn, bchain, btag, hedge] = ...
                                        this.import_pet_topology(e,t);
                this.hedge = hedge;
                %generate complex
                for k = 1:(this.n+1)
                    this.d{k} = sparse(crow{k}, bcol{k}, dorn{k});
                    this.C{k} = kChainSpace(k,nt(k),bchain{k}, btag{k});
                end 
            end
        end
        function [nt, crow, bcol, dorn, bchain, btag, hedge] = ...
                                import_pet_topology(this, e, t)
            %This function extracts the implicit topologic information
            %contained in e and t. Namely, this function:
            % i)    generates the edge entities from the set of vertices of t
            % ii)   maps periodic vertices, reindexing and removing
            %       duplicates, if necessary
            % iii)  index chain-boundary relations
            % iv)   tag boundary k-chains

            % i)    GENERATE EDGES
            vp = [];                                    %vertex pairs (vp)
            for i=1:size(t,1)                           %get all vp in chain
                vp = [vp;t([1,2],:)'];
                t  = circshift(t,-1);
            end
                                 
            % ii.a) REMAP VERTICES
            %obtain map from periodic B.C. specified in e
            pmidx   = e([3,4],:)~=0 & e([3,4],:)~=e([1,2],:);
            map     = [e([3,1], pmidx(1,:)), e([4,2],pmidx(2,:))];
            map     = unique(map,'rows');
            map     = sortrows(map');                        
            
            % get healing vertices
            hvp     = e([1 2], e(3,:)~=0 & e(3,:)~=e(1,:))';
            heidx   = ismember(sort(vp,2), sort(hvp,2), 'rows');
            
            if numel(map)
                %NOTE: We apply changem x2 for double periodic B.C. (think
                %about corner nodes, which undergo a double connection)
                %remap vertices
                vp      = changem(vp, map(:,1), map(:,2));
                vp      = changem(vp, map(:,1), map(:,2));
            end
            
            % ii.b) REINDEXING
            uvert   = unique(vp);                           %used vert
            nvert   = numel(uvert);                         %num of used vert
            vp      = changem(vp,1:nvert, uvert);            
            
            % ii.c) REMOVE DUPLICATES            
            [~,uidx]    = unique(sort(vp,2),'rows');
            uvp         = sortrows(vp(uidx,:));
            
            % ii.d) INDEX HEALING EDGES
            hvp     = sort(vp(heidx,:),2);
            hedge   = ismember(hvp,uvp,'rows');

            % iii)  CELL-BOUNDARY INDEXING
            %total number of k-chains
            nt = [nvert, size(uvp,1), size(t,2)];
            %chain indexing (row index)
            crow{1}     = repmat(1:nt(2),[1,2])';           %vert indexing
            crow{2}     = repmat(1:nt(3),[1,size(t,1)])';   %edge indexing
            crow{3}     = [];                               %face indexing
            %boundary indexing (column index)
            bcol{1}     = uvp;                          %vert -> edge
            [~,bcol{2}] = ismember(sort(vp,2),sort(uvp,2),'rows');            
            bcol{3}     = [];
            %boundary orientation (value)
            dorn{1}     = repmat([-1,1], nt(2),1);
            dorn{2}     = (vp(:,1) == uvp(bcol{2},1))*2-1;
            dorn{3}     = [];
            
            % iv)   TAG BOUNDARY K-CELLS
            bidx = e(3,:)==0 | e(4,:)==0;
            btag{1} = [];
            btag{2} = e(5,bidx)';
            btag{3} = [];
            bvp  = e([1 2],bidx)';
            bvp  = sort(bvp,2);
            %periodic remapping of vert
            if numel(map)
                bvp = changem(bvp, map(:,1), map(:,2));
                bvp = changem(bvp, map(:,1), map(:,2));
            end
            %vert reindexing
            bvp             = changem(bvp, 1:nvert, uvert);
            bchain{1}       = unique(bvp','stable');
            [~,bchain{2}]   = ismember(sort(bvp,2),sort(uvp,2),'rows');
            bchain{3}       = [];
        end
        function obj = dual(this)
            % Generates a dual cell complex out of the primal one as
            % follows:
            % i)    Complex Generation
            %       Dualize primal spaces to allocate the dual ones.
            %       Translate boundary tags to the dual spaces.
            % ii)   Differential parsing
            %       Exploit the relation between the primal and dual
            %       differentials as: d_dual_k = d_primal_(n-k)^T.
            %       The k-complex is completed by attaching the primal 
            %       (n-k)-boundary nodes. Namely, Tr_primal_(n-k)^T is
            %       attached.
            
            % i)    GENERATE DUAL COMPLEX
            obj = ChainComplex(this.n);
            for kd = 1:(obj.n+1)
                kp = 2+this.n-kd;
                nchains     = this.C{kp}.nchains + this.C{kd}.nbndary;
                bchains     = 1+(this.C{kp}.nchains:(nchains-1));
                btag        = this.C{kp}.tbndary;
                obj.C{kd}   = kChainSpace(kd, nchains, bchains, btag);
            end
            
            % ii)   PARSE DIFFERENTIALS
            for kd = 1:obj.n
                kp = 1+this.n-kd;
                %transpose primal differentials + trace
                obj.d{kd}   = [-this.d{kp}' this.C{kp}.Tr'];
            end
            %obtain boundary-boundary incidence
            for kd = 1:(obj.n-1)
                kp = 1+this.n-kd;
                dbb         = this.C{kp}.Tr*this.d{kd}*this.C{kd}.Tr';
                zii         = zeros(size(dbb,1), size(this.d{kp},1));
                obj.d{kd}   = [obj.d{kd}; zii dbb'];
            end
        end    
    end
end

