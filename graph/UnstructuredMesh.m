function [p,e,t] = UnstructuredMesh(Hmax,connection)
%Unstructured Triangular Mesh

% [p,e,t] = initmesh(@squareg,'Hmax',Hmax,'Jiggle','minimum','JiggleIter',10);
% p=0.5*(p'+[1,1])';
% [p,e,t] = initmesh(@rectangleg,'Hmax',Hmax,'Jiggle','minimum','JiggleIter',10);
[p,e,t] = initmesh(@circleg,'Hmax',Hmax,'Jiggle','minimum','JiggleIter',10);
% p=0.5*(p'+[1,1])';

min(pdetriq(p,t))
pdemesh(p,e,t,'NodeLabels','on','ElementLabels','on');
% savevtk(p,t,'mesh.vtk');

t(end,:)=[];                                %last row is useless: remove
for k = 1:2:size(connection,2)
    e([3,4],e(5,:) == connection(k)) = flip(flip(e([1 2],e(5,:) == connection(k+1))')');
end
end