function [p,e,t] = CartesianMesh(nx,ny,conn,filename)
n  = 2;
Lx = 2;
Ly = 2;
% Ly = Lx;
x0 = -Lx/2;
y0 = -Ly/2;
%Cartesian Mesh
p=zeros(2,nx*ny);
e=zeros(7,2*nx+2*ny);
t=ones(4,nx*ny);
for i=0:nx
    for j=0:ny
        %define points
        p(1,1+i+j*(nx+1)) = x0 + Lx*i/nx;
        p(2,1+i+j*(nx+1)) = y0 + Ly*j/ny;
        %define cells
        if (i>0 && j>0)
            c=i+(j-1)*nx;        
            t(1,c) = i  +   (j-1)*(nx+1);
            t(2,c) = i+1+   (j-1)*(nx+1);
            t(3,c) = i+1+   (j  )*(nx+1);
            t(4,c) = i  +   (j  )*(nx+1);
        end
    end
end
vi  = [(nx+1)*ny+(1:nx) (nx+1)*(ny+1):-(nx+1):2*(nx+1) nx+1:-1:2 1:nx+1:(nx+1)*ny];
vf  = vi + [ones(1,nx), -(nx+1)*ones(1,ny), -ones(1,nx), (nx+1)*ones(1,ny)];
tag = [ones(1,nx), 2*ones(1,ny), 3*ones(1,nx), 4*ones(1,ny)];
e([1 2 5 7],:) = [vi;vf;tag;ones(1,2*(nx+ny))];
for k = 1:2:size(conn,2)
    tgtidx = e(5,:) == conn(k);
    srcidx = e(5,:) == conn(k+1);
    linkvert = flip(flip(e([1 2], srcidx)')');
    e([3,4],tgtidx) = flip(flip(e([1 2], srcidx)')');
    e([3,4],srcidx) = e([1 2], srcidx);
end
save(filename,'p','e','t');
% mkdir(odir);
% savevtk(p,t,strcat(odir,'/mesh.vtk'));
end
function savevtk(p,t,filename)
fprintf('saving %s...',filename);
fid = fopen(filename,'w');
fprintf( fid,'# vtk DataFile Version 3.0\n');
fprintf( fid,'2D rectangular mesh\n');
fprintf( fid,'ASCII\n');
fprintf( fid,'DATASET UNSTRUCTURED_GRID\n\n');
fprintf( fid,'POINTS %i FLOAT\n',size(p',1));
fprintf( fid,'%f %f 0.0\n', p);
fprintf( fid,'\nCELLS %i %i\n', size(t',1), size(t',1)*5);
fprintf( fid,'4 %i %i %i %i\n',t(1:4,:)-1);
fprintf( fid,'\nCELL_TYPES %i\n', size(t',1));
fprintf( fid,'%i \n',[9*ones(size(t',1),1)]);
% fprintf( fid,'\n\nCELL_DATA %i\n', size(t',1));
% fprintf( fid,'SCALARS variable float 1\n');
% fprintf( fid,'LOOKUP TABLE default\n');
% fprintf( fid,'%f\n',ones(size(p',1),1));
fclose(fid);
fprintf('OK\n')
end
