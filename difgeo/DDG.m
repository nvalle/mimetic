classdef DDG < ChainComplex
    %DDG Discrete Differential Geometry
    %   DDG contains all the geometric properties of any chain complex and
    %   thus enriches it with new quantities.
    
    properties
        range               % domain range
        M                   % metric
        x                   % position
        uvec                % unit vector
    end
    
    methods
        function this = DDG(varargin)
            %DDG Construct an instance of DDG
            %   n   : number of dimensions
            %   mesh: filename
            this@ChainComplex(varargin{:});
            if nargin > 1
                meshname = varargin{2};
                %load p, e, t from file
                load(meshname);
                %import geometry
                this.import_pet_geometry(p,e,t);
            end
        end
        function import_pet_geometry(this,p,e,t)
            this.range      = range(p');
            %get baricentric positions
            this.x{1}     = p';
            %remove periodic vertices
            idx             = unique(e([1,2],e(3,:)>0 & e(1,:)~=e(3,:)));
            this.x{1}(idx,:)= [];
            %get distance
            dx              = this.distance(this.x{1});
%             this.x{2}       = (this.d{1}<0)*this.x{1} + 0.5*dx;
            this.x{2}       = this.position(this.x{1});
            %compute centroid
            xv              = reshape(p(1,t)',size(t));
            yv              = reshape(p(2,t)',size(t));
            this.x{3}       = [sum(xv); sum(yv)]'/size(t,1)';            
            %set metric
            A               = polyarea(xv,yv)';            
            L               = vecnorm(dx,2,2);
            this.M{1}       = speye(this.C{1}.nchains);
            this.M{2}       = spdiags(L,0,this.C{2}.nchains,this.C{2}.nchains);
            this.M{3}       = spdiags(A,0,this.C{3}.nchains,this.C{3}.nchains);
            %set uvec
            this.uvec{1}    = ones(size(this.x{1},1),1);
            this.uvec{2}    = this.M{2}\dx;
            this.uvec{3}    = ones(size(this.x{3},1),1);
        end
        function obj = dual(this)
            % Generates a dual cell complex out of the primal one as
            % follows:
            %   i)  ChainComplex (topologic) dualization
            %       Because of the MATLAB limitted support for class
            %       constructors, we need to create and intermedia 'aux'
            %       ChainComplex instance to hold the topological
            %       dualization values to the ChianComplex components of
            %       DDG. This may be revised in future versions.
            %   ii) Metric dualization
            
            % i)    TOPOLOGIC DUALIZATION
            obj = DDG(this.n);
            aux = dual@ChainComplex(this);
            obj.n = aux.n;
            obj.C = aux.C;
            obj.d = aux.d;
            
            % ii)   METRIC DUALIZATION
            obj.range = this.range;
            %assign x{k} to x{n-k}
            for k = 1:this.n+1
                obj.x{k} = this.x{this.n+2-k};
            end
            %process dual boundaries
            for kd = 1:obj.n
                kp = 1+this.n-kd;
                obj.x{kd} = [obj.x{kd}; this.C{kp}.Tr*this.x{kp}];
            end
            dx          = obj.distance(obj.x{1});
            %fix periodic boundary conditions
            L           = vecnorm(dx,2,2);
            A           = sum(1/8*(abs(this.d{2})*abs(this.d{1}))'*this.M{3},2);
            %set metric
            obj.M{1}    = speye(obj.C{1}.nchains);
            obj.M{2}    = spdiags(L,0,numel(L),numel(L));
            obj.M{3}    = spdiags(A,0,obj.C{3}.nchains,obj.C{3}.nchains);
            %set uvec
            obj.uvec{1} = ones(size(obj.x{1},1),1);
            obj.uvec{2} = obj.M{2}\dx;
            obj.uvec{3} = ones(size(obj.x{3},1),1);
        end
        function dx  = distance(this,x)
            %distance compute distance in a periodic domain
            %   Distances are defined as the shortest distance between two
            %   neighboring cells. While the naive approach works fine for
            %   non-periodic connections, distances computed directly
            %   across a periodic provide a bigger value than half the
            %   domain 'window', meaning that the right direction would be
            %   arround the domain and not within the given window. This is
            %   the condition checked and fixed in within this function to
            %   return a function which is properly defined.
            %   x : node position
            
            %compute naive distance
            dx              = this.d{1}*x;
            %find patologic edges
            pidx            = abs(dx)>(0.5*abs(this.range));
            %determine sign of the pathological distance
            sx              = sign(dx(pidx(:,1),1));
            sy              = sign(dx(pidx(:,2),2));
            %correct distances for all dimensions
            dx(pidx(:,1),1) = dx(pidx(:,1),1) - sx*this.range(:,1);
            dx(pidx(:,2),2) = dx(pidx(:,2),2) - sy*this.range(:,2);
        end
        function xbc = position(this,x)
            %position compute the average position in a periodic domain
            %   Positio
            %   x : boundary positions
            xbc = ((this.d{1}>0)-1/2*this.d{1})*x;
        end
    end
end

