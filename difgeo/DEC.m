classdef DEC < DoubleComplex
    %DEC Discrete Exterior Calculus
    %   DEC contains the implementation of fundamental exterior calculus
    %   operations over a discrete double complex. Most notably, the
    %   Hodge-star operator is given as the matrix H : (k)Cp -> (n-k)Cd.
    %   Note that in the presence of non-vanishing boundary conditions,
    %   while duality between primal and dual spaces impose same
    %   dimension (i.e., degrees of freedom), the number of elements is
    %   different. This is due to the presence of additional boundary
    %   nodes. These extra degrees of freedom are removed, however, when
    %   imposing boundary conditions, which are formulated in terms of the
    %   Trace (Tr) operator by the final user.
    
    properties (Access = public)
        Hpd                 % Hodge-star operator primal    -> dual
        Hdp                 % Hodge-star operator dual      -> primal
    end
    
    methods
        function this = DEC(n,mesh)
            %DEC Constructor
            %   DEC inherits a new instance of DoubleComplex and specifies
            %   both the metric relations (i.e., Hodge-star operators) as
            %   well as the interior product relations. This last one
            %   requires the definition of several 
            this@DoubleComplex(n,mesh);
            for kp=1:(this.n+1)
                kd = 2 + this.n - kp;
                m = this.Cp.C{kp}.nchains;
                n = this.Cd.C{kd}.nchains;
                this.Hdp{kd} = this.Cp.M{kp}\speye(m,n)*this.Cd.M{kd};
                this.Hpd{kp} = this.Cd.M{kd}\speye(n,m)*this.Cp.M{kp};
            end
        end
    end
    methods (Access = protected)
        function p = ip(this,u,v)
            %ip Interior product
            %   Computes the interior product of a vector field and a
            %   differential form. ip(v,·) is an antiderivation as:
            %   ip(v,·) : k-forms -> (k-1)-forms.
            %   At this point the interior product is only defined for
            %   coupling vector fields with n-forms by means of a typical
            %   finite volume interpolator i : n-form -> (n-1)-form
            %   u : vector field
            %   v : n-form
            %   i : n-form -> (n-1)-form 
            p = u.*i(v);
        end
        function l = Lie(this,u,v,i)
            %Lie Lie derivative
            %   Computes the Lie the derivative of a differential form
            %   along a vector field. Note that this is only implemented
            %   for n-valued chains, although this is work in progress.
            %   u : vector field 
            %   v : differential n-form
            %   i : form interpolator (n-form -> (n-1)-form)
            l  = this.d{2}*this.ip(u,v,i);
        end
    end
    methods
        function i = SP(this,u)
            i = 1/2 * abs(this.Cd.d{2})*u;
        end
        function i = UP(this,u)
            i = (this.Cd.d{2}>0)*max(u,0) + (this.Cd.d{2}<0)*min(u,0);
        end
    end
end

