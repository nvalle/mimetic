classdef (Abstract) DVC < DEC
    %DVC Discrete Vector Calculus
    %   DVC contains the virtual implementation of fundamental vector calculus
    %   operations as linear maps. These are implemented in later derived
    %   classes.
    properties

    end
    
    methods
        function this = DVC(n,mesh)
            %DVC Constructor
            %   DVC inherits a new instance of DEC on which the vector
            %   calculus operators are defined.
            this@DEC(n,mesh);
        end
        function c = ADV(this,u,v,i)
            %A Advection
            %   Computes the advection of a differential form along a
            %   vector-valued velocity field.
            %   u : velocity field
            %   v : scalar field
            %   i : interpolation scheme
            c = this.Lie(u,v,i);
        end
    end
    methods (Abstract)
        g = grad(this,u)
        r = curl(this,u)
        d = div(this,u)
        u = init(this,k,d)
    end
end

