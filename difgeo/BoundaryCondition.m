classdef BoundaryCondition
    %BoundaryCondition Defines a virtual class suitable to handle generic
    %boundary conditions.
    %   BoundaryCondition contains the basic information required to
    %   implement any boundary condition. Considering that any boundary
    %   condition can be cast into an implicit algebraic form as Ax = b,
    %   the particular implementation is up to the derived classes or the
    %   user in custom implementations.
    
    properties
        bidx    %boundary index
        A       %Linear operator
        b       %Independent term
    end
    
    methods (Access = protected)
        function this = BoundaryCondition(A,b)
            %BoundaryCondition Generic boundary condition constructor
            %   Constructs a boundary condition imposed as Ax = b, where A
            %   contains the type of boundary and b is the independent
            %   term.
            %   A : Boundary constrain
            %   b : Boundary value
            this.A = A;
            this.b = b;
        end
    end
    methods (Access = public)
        function u = UpdateBoundary(this,u)
            %UpdateBoundary Updates the boundary entries of u
            %   Update the boundary elements of u such that they satisfy 
            %   Au = b
            u_b = this.A\b;
            u([bidx:end],:) = u_b;
        end
    end
end

