classdef DoubleComplex
    %DoubleComplex contains a duality of CellComplexes and its metrics
    %   A double complex is comprised of primal (C_p) and dual (C-d) cell
    %   complexes. Note that the number of elements of the
    %   dual of Cp[k] and Cd[n-k] is not the same if there are
    %   non-topological (i.e., non-periodic) boundary conditions. This
    %   gives rise naturally to the boundary cells, which are used to
    %   impose boundary conditions trough the Tr operator, defined for
    %   every kCellSpace, and required to close an eventual system of
    %   equations.
    properties
        n                   % embedding dimension
        Cp                  % primal cell complex
        Cd                  % dual cell complex      
    end
    methods
        function this = DoubleComplex(n,mesh)
            this.n       = n;
            this.Cp      = DDG(n,mesh);
            this.Cd      = this.Cp.dual();
        end
    end
end

