function [ psi ] = MINMOD( rf )
%Bart-Jespersen flux limiter
    psi = max(0,min(1,rf));
end

