function [ psi ] = MAXMOD( rf )
%MAXMOD flux limiter (not 2nd order)
    psi = max(0,min(2*rf,2));
end