classdef FL
    %FL Flux Limiter
    %   Flux limiters are used to capture sharp discontinuities in a
    %   transport equation. It provides stability by including some
    %   numerical viscosity. Such aritficial viscosity is computed
    %   according to a Flux Limiter scheme, which is provided as argument
    %   to the FL object constructor.
    properties
        M;          %associated Manifold
        sch;        %function handle scheme
        uuD;        %unoriented upwind delta
        ouD;        %oriented upwind delta
    end
    methods
        function fl = FL(M,scheme)
            fl.M    = M;
            fl.sch  = scheme;
            %indexing variables to construct dx matrix projection
            Ie  = speye(fl.M.T.ne);
            Id  = speye(fl.M.T.d);
            %construction of upstream delta matrices
            ofA     = M.T.PT*(    M.T.Tc' *abs(M.T.Tc)-2*Ie + M.T.deg);
            ufA     = M.T.PT*(abs(M.T.Tc')*abs(M.T.Tc)-M.T.deg+M.T.PF);
            fl.uuD  = 1/2*M.T.N*kron(Id,ufA)*M.T.N'*M.T.D;
            fl.ouD  = 1/2*M.T.N*kron(Id,ofA)*M.T.N'*M.T.D;
        end
        function rf = GR(fl,theta,Q) %Gradient Ratio
%             d  = Q*fl.M.T.D*theta;
%             d(abs(d)<1E-6)=1;
%             aux= spdiags(d,0,fl.M.T.ne,fl.M.T.ne);          
%             rf = aux\((Q*fl.uuD-fl.ouD)*theta);
            rf = (Q*fl.M.T.D*theta).\((Q*fl.uuD-fl.ouD)*theta);
        end
        function w = F(fl,u,theta)
            Q     = spdiags(sign(u(:)),0,fl.M.T.ne,fl.M.T.ne);
            w     = 1/2*spdiags(fl.sch(fl.GR(theta,Q))-1,0,fl.M.T.ne,fl.M.T.ne)*Q*fl.M.T.D;
%             w     = 1/2*(fl.sch(fl.GR(theta,Q))-1).*Q*fl.M.T.D; %SLOWER
        end
    end
end

