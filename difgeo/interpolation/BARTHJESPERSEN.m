function [ psi ] = BARTHJESPERSEN( rf )
%Bart-Jespersen flux limiter
    psi = 0.5*(rf+1).*min(min(1,4*rf./(rf+1)),min(1,4./(rf+1)));
end

