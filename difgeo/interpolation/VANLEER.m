function [ psi ] = VANLEER( rf )
%SUPERBEE flux limiter
    psi = (rf+abs(rf))./(1+abs(rf));
end

