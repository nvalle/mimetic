function [ psi ] = KOREN( rf )
%Bart-Jespersen flux limiter
    psi = max(0,min(2*rf,min((2+rf)./3,2)));
end

