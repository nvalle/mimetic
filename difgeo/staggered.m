classdef staggered < DVC
    %staggered Staggered arrangement of a Discrete Vector Calculus object
    %   staggered implements a DVC object which considers a staggered
    %   arrangement of the variables, which is particualarly useful for
    %   vector-valued fields.
    %   If collocated arrangements locate scalars at at the dual vertices,
    %   staggered arrangements locate vector-valued variables at the dual
    %   edges.
    %   Balance laws are computed at the corresponding primal locations.
    %   Quantities are arranged as follows:
    %   0-forms : vectors           -> dual edges (across primal areas)
    %   1-forms : vector gradients  -> dual vertices + primal volumes
    %   2-forms : vector fluxes     -> primal faces
    %   3-forms : cons. quantities  -> primal volumes
    %   Discrete vector calculus operations are defined accordingly and
    %   defined in the 'properties' section.
    
    properties
        GRAD        % Gradient      : dual_vert     ->  primal_face
        CURL        % Curl          : dual_edge     ->  primal_vert
        DIV         % Divergence    : primal_face   ->  dual_vert
        LAP         % Laplacian     : dual_vert     ->  dual_vert
        %NOTE: Divergence and Laplacian are defined over the dual vertices,
        %which imply the boundary vertices. Consequently, the images of both
        %D and L at the boundaries are 0. This is perfectly fine as far as
        %we impose the boundary conditions there.
    end
    
    methods
        function this = staggered(n,mesh)
            %staggered Constructor
            %   staggered inherits a new instance of DVC on which the vector
            %   calculus operators are defined.
            this@DVC(n,mesh);
            this.GRAD = this.Cd.M{2}\this.Cd.d{1}*this.Cd.M{1};
            this.CURL = this.Cd.d{2}*this.Cd.M{2};
            this.DIV  = this.Cp.M{3}\this.Cp.d{this.n}*this.Cp.M{this.n};
%             this.LAP  = this.DIV*this.GRAD;
        end
        function g = grad(this,u)
            g = this.GRAD*u;
        end
        function r = curl(this,u)
            r = this.CURL*u;
        end
        function d = div(this,u)
            d = this.DIV*u;
        end
        function u = init(this,k,d)
            u = zeros(this.Cd.C{k}.nchains,d);
        end
    end
end

