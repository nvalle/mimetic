classdef Dirichlet < BoundaryCondition
    %Dirichlet Dirichlet boundary condition
    %   Dirichlet imposes a Dirichlet boundary condition for all the
    %   boundary elements tagged in the trace operator.    
    properties
    
    end
    
    methods
        function this = Dirichlet(M,varargin)
            %Dirichlet Construct an instance of this Dirichlet
            %   Contruct a specialized constructor of BoundaryCondition.
            %   Assuming a condition of type Ax = b, A is Tr and b is the
            %   value.
            A = M.Cd.C{1}.Tr;
            if nargin > 1
                b = varargin{1};
            else
                b = zeros(M.Cd.C{1}.nbndary,1);
            end
            this@BoundaryCondition(A,b);
        end
    end
end

