%Ellipse definition
r=1;
a=0.5;
b=0.3;
%ellipse plotting
[X,Y]=meshgrid(linspace(-1,1),linspace(-1,1));
contour(X,Y,a^-2*X.^2+b^-2*Y.^2,[r^2,r^2],'ShowText','on')
%distance from point to ellipse
d=@(th,x)(x-[r*a*cos(th),r*b*sin(th)]);
%target function to optimize
t=@(th,x)(r*(a^2-b^2).*sin(th).*cos(th)-x(:,1)*a*sin(th)+x(:,2)*b*cos(th));

%protoype test
o=pi/2*rand(10,1);
dx=0.4;
x=[(r*a+dx)*cos(o),(r*b+dx)*sin(o)];
%embeed the target function into single-argument function f
options = optimset('Display','iter');
[t,rp] = cart2pol(x(:,1),x(:,2));
for i=1:size(x,1)
    f     = @(th)t(th,x(i,:));            
    th(i) = fzero(f,t(i),options);         %MORE OP.
%     f     = @(th)norm(d(th,x(i,:)));
%     th(i) = fminsearch(f,atan(a*x(i,2)/(b*x(i,1))),options);    %LESS OP.
end
dx=vecnorm(d(th',x),2,2)