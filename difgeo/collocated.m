classdef collocated < DVC
    %collocated Collocated arrangement of a Discrete Vector Calculus object
    %   collocated implements a DVC object which considers a collocated
    %   arrangement of the variables, i.e., all variables are located at
    %   the vertices of the dual grid, while the balance laws are computed
    %   at the n-chains of the primal grid.
    %   Quantities are arranged as follows:
    %   0-forms : scalars           -> dual vertices (centroids primal areas)
    %   1-forms : gradients         -> dual edges (across primal faces)
    %   2-forms : fluxes            -> primal faces
    %   3-forms : cons. quantities  -> primal volumes
    %   Discrete vector calculus operations are defined accordingly and
    %   defined in the 'properties' section.
    
    properties
        GRAD        % Gradient      : dual_vert     ->  primal_face
        CURL        % Curl          : dual_edge     ->  primal_vert
        DIV         % Divergence    : primal_face   ->  dual_vert
        LAP         % Laplacian     : dual_vert     ->  dual_vert
        %NOTE: Divergence and Laplacian are defined over the dual vertices,
        %which imply the boundary vertices. Consequently, the images of both
        %D and L at the boundaries are 0. This is perfectly fine as far as
        %we impose the boundary conditions there.
    end
    
    methods
        function this = collocated(n,mesh)
            %collocated Constructor
            %   collocated inherits a new instance of DVC on which the vector
            %   calculus operators are defined.
            this@DVC(n,mesh);
            this.GRAD = this.Cd.M{2}\this.Cd.d{1}*this.Cd.M{1};
            this.CURL = this.Cd.d{2}*this.Cd.M{2};
            this.DIV  = this.Cp.M{3}\this.Cp.d{this.n}*this.Cp.M{this.n};
%             this.LAP  = this.DIV*this.GRAD;
        end
        function g = grad(this,u)
            g = this.GRAD*u;
        end
        function r = curl(this,u)
            r = this.CURL*u;
        end
        function d = div(this,u)
            d = this.DIV*u;
        end
        function u = init(this,k,d)
            u = zeros(this.Cd.C{k}.nchains,d);
        end
    end
end

