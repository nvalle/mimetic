%Rhodorea definition
r0=1;
r1=0.5;
w=5;
r=@(th)r0+r1*sin(w*th);
%plotting
th = linspace(0,2*pi);
plot(r(th).*cos(th),r(th).*sin(th))
%distance from point to rhodorea
d=@(th,x)(x-[r(th).*cos(th),r(th).*sin(th)]);
clear th
%protoype test
o=pi/2*rand(10,1);
dx=0.4;
x=[(r(o)+dx).*cos(o),(r(o)+dx).*sin(o)];
%embeed the target function into single-argument function f
options = optimset('Display','iter');
[t,rp] = cart2pol(x(:,1),x(:,2));
for i=1:size(x,1)
%     f     = @(th)t(th,x(i,:));            
%     th(i) = fzero(f,atan(a*x(i,2)/(b*x(i,1))),options);         %MORE OP.
    f     = @(th)norm(d(th,x(i,:)));
    th(i) = fminsearch(f,t(i),options);    %LESS OP.
end
dx=vecnorm(d(th',x),2,2)
% hold on;
% plot(x(:,1),x(:,2),'o')
% plot(r(th).*cos(th),r(th).*sin(th),'*')
