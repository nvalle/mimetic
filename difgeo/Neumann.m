classdef Neumann < BoundaryCondition
    %Neumann Neumann boundary condition
    %   Neumann imposes a Neumann boundary condition for all the
    %   boundary elements tagged in the trace operator.    
    properties
    
    end
    
    methods
        function this = Neumann(M,varargin)
            %Neumann Construct an instance of Neumann boundary conditions
            %   Contruct a specialized constructor of BoundaryCondition.
            %   Assuming a condition of type Ax = b, A is Tr and b is the
            %   value.
            A = M.Cp.C{2}.Tr*M.Hdp{2}*M.GRAD;
            if nargin > 1
                b = varargin{1};
            else
                b = zeros(M.Cd.C{2}.nbndary,1);
            end
            this@BoundaryCondition(A,b);
        end
    end
end

