classdef FSM
    %FSM Fractional Step Method
    %   The fractional step method is used to decouple a solenoidal vector
    %   field (i.e., divergence free) from an irrotational one (e.g., a
    %   scalar potential). This is based in the Helmholtz-Hodge
    %   decomposition and composed of the following steps:
    %   i)  Prediction.
    %       The solenoidal field is advanced in time according to the
    %       governing equation, irrespective of the solenoidal constrain,
    %       and thus potentially breaking it.
    %   ii) Projection.
    %       The intermediate vector field, the predicted one, is projected
    %       over the solenoidal (i.e., divergence-free) space. This
    %       projection is assigned to the irrotational field.
    %   iii)Correction.
    %       The irrotational field is subtracted from the predicted one in
    %       order to obtain a corrected one which satisfies the solenoidal
    %       constrain.
properties
    M                                               %Manifold
    eq                                              %equation
    Predictor                                       %ODE solver        
    Projector                                       %Compatibility equation
    PSolver                                         %Algebraic solver
    maxdt                                           %minimum time step
    MM                                              %Momentum Matrix
end
methods
    function f = FSM(eq,varargin)
        f.M         = eq.M;
        f.eq        = eq;
        f.Predictor = AB2(@eq.R,varargin{:});
        A           = -eq.M.T.Vc*eq.M.LAP;        
%         index       = diag(f.M.T.BF*f.M.T.PF<0);
%         B           = f.M.T.S(index,index);
%         f.Projector = PFD(A,B);
%         A(1,1)      = 1.1*A(1,1);
        f.Projector = CHOL(A);
%         f.Projector = PCG(A,ichol(A,struct('michol','on')));
        f.maxdt     = full((min(diag(f.M.T.Dx))^3/(2*pi()*eq.ceq.st))^(1/2));
        f.MM        = kron(f.M.T.Id,ones(1,f.M.T.ne))*f.M.T.N'*f.M.T.Vs;
    end
    function [r,Ek,momentum,uc,P] = step(f,u,P,th,n)
        rhoc    = f.eq.ceq.rhoc(th);
        dt      = f.Predictor.dt;
        [~,up]  = f.Predictor.step(u,P,th,n);               %predict
%         P       = f.Projector.solve(-f.M.T.Vc*f.M.DIV*up);  %project %WARNING!
        P       = f.Projector.solve(f.b(up,th,dt));         %project %WARNING!
        uc      = up - f.M.GRAD*P;                          %correct
        divU    = vecnorm(f.M.DIV*uc,Inf);
        r       = norm(uc-u,Inf)/dt;                        %error
        Ek      = 1/2*uc'*f.M.T.Vs*uc;                      %energy
        momentum= (f.MM*(uc.*uc))';                         %momentum
    end
    function b = b(f,up,th,dt)
        b = -f.M.T.Vc*f.M.DIV*(up);
    end
    function setdt(f,dt)
        f.Predictor.setdt(dt);
    end
end
end
