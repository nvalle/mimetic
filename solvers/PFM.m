classdef PFM % < integrator
    %Phase Field  Method
properties
    M                       %Manifold
    ICapturing              %Interface Capturing method
    Predictor               %predictor step (transport equation)
end
methods
    function f = PFM(eq,u,th)
        f.M             = eq.M;
        f.ICapturing    = eq;
        f.Predictor     = AB2(f.ICapturing,th,u);
        dt              = min(f.M.T.Dx^2*ones(f.M.T.ne,1));
        f.Predictor.setdt(dt);
    end
    function [r,Ep,mass,th,n] = step(f,u,p,th0,n)       
        [~,th] = f.Predictor.step(th0,u);               %predict
        n      = f.ICapturing.normal(th);               %normal
        k      = f.ICapturing.curvature(u,th);          %curvature
        r      = vecnorm((th-th0)/f.Predictor.dt,Inf);  %error
        Ep     = n'*f.M.T.Vs*f.M.GRAD*th;               %energy
        mass   = ones(size(th))'*f.M.T.Vc*th;           %mass
    end
    function setdt(f,dt)
        f.Predictor.setdt(dt);
    end
end
end
