classdef LSM % < integrator
    %Level-Set  Method
properties
    M                       %Manifold
    IC                      %Interface Capturing method (Level Set)
    Predictor               %predictor step (trasnsport equation)
    Corrector               %corrector step (reinitialization equation)
    maxdt                   %maximum time step
    ceq                     %constitutive equation
end
methods
    function f = LSM(eq,ceq,u,th,nr)
        f.M             = eq.M;
        f.IC            = eq;       %Interface Capturing equation
        f.Predictor     = AB2(@eq.ADV,th,u);
        f.Corrector     = RK3(@eq.RSH,th,f.IC.normal(th),u);
        f.Corrector.smax= nr;
        f.maxdt         = full(min(diag(eq.epsilon)));
        f.Predictor.setdt(f.maxdt);
        f.Corrector.setdt(min(diag(eq.epsilon)));
        f.ceq           = ceq;
    end
    function [r,Ep,mass,th,n] = step(f,u,p,th0,n)       
%         n      = f.ICapturing.normal(th0);               %normal TEST
%         th     = th0;                                    %marker TEST
        [~,th] = f.Predictor.step(th0,u);               %predict
        n      = f.IC.normal(th);                       %normal
        [~,th] = f.Corrector.solve(th,n,u);             %correct
        k      = f.IC.curvature(u,th);                  %curvature
        r      = vecnorm((th-th0)/f.Predictor.dt,Inf);  %error
        Ep     = f.ceq.st*n'*f.M.T.Vs*f.M.GRAD*th;      %energy
        mass   = ones(size(th))'*f.M.T.Vc*th;           %mass
    end
    function setdt(f,dt)
        f.Predictor.setdt(dt);
    end
end
end