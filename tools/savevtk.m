function savevtk(p,t,filename)
%savematrix stores sparse matrix in COO format
%   the index, column and valor rows will be stored in filename.csv
fprintf('saving %s...',filename);
fid = fopen(filename,'w');
fprintf( fid,'# vtk DataFile Version 3.0\n');
fprintf( fid,'2D scalar data\n');
fprintf( fid,'ASCII\n');
fprintf( fid,'DATASET UNSTRUCTURED_GRID\n\n');
fprintf( fid,'POINTS %i FLOAT\n',size(p',1));
fprintf( fid,'%f %f 0.0\n', p);
fprintf( fid,'\nCELLS %i %i\n', size(t',1), size(t',1)*4);
fprintf( fid,'3 %i %i %i\n',t(1:3,:)-1);
fprintf( fid,'\nCELL_TYPES %i\n', size(t',1));
fprintf( fid,'%i \n',[5*ones(size(t',1),1)]);
% fprintf( fid,'\n\nCELL_DATA %i\n', size(t',1));
% fprintf( fid,'SCALARS variable float 1\n');
% fprintf( fid,'LOOKUP TABLE default\n');
% fprintf( fid,'%f\n',ones(size(p',1),1));
fclose(fid);
fprintf('OK\n')
end
