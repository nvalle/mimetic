function savematrix(A,filename)
%savematrix stores sparse matrix in COO format
%   the index, column and valor rows will be stored in filename.csv
fprintf('saving %s...',filename)
[i,j,val] = find(A);
fid = fopen(filename,'w');
fprintf( fid,'%d;%d;%f\n', [i,j,val]' );
fclose(fid);
fprintf('OK\n')
end

