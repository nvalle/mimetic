function savevector(x,filename)
%savematrix stores sparse matrix in COO format
%   the index, column and valor rows will be stored in filename.csv
fprintf('saving %s...',filename)
fid = fopen(filename,'w');
fprintf( fid,'%f\n', x' );
fclose(fid);
fprintf('OK\n')
end
