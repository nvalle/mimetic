function savevectorfield(x,filename,meshname)
%savematrix stores sparse matrix in COO format
%   the index, column and valor rows will be stored in filename.csv

fprintf('saving %s...',filename)
copyfile(meshname,filename);
fid = fopen(filename,'a');
fprintf( fid,'\n\nCELL_DATA %i\n', size(x,1));
fprintf( fid,'FIELD attributes 1 vector %i %i float\n', size(x,2), size(x,1));
fprintf( fid,'%f %f\n', x' );
fclose(fid);
fprintf('OK\n')
end
