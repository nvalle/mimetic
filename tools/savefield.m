function savefield(x,filename,meshname)
%savematrix stores sparse matrix in COO format
%   the index, column and valor rows will be stored in filename.csv

fprintf('saving %s...',filename)
copyfile(meshname,filename);
fid = fopen(filename,'a');
fprintf( fid,'\n\nCELL_DATA %i\n', size(x,1));
fprintf( fid,'SCALARS variable float %i\n', size(x,2));
fprintf( fid,'LOOKUP_TABLE default\n');
fprintf( fid,'%f\n', x' );
fclose(fid);
fprintf('OK\n')
end
