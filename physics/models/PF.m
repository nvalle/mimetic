classdef PF < handle
    %Phase Field
properties
    M               %Manifold over which the equation is formulated
    E               %diffusivity matrix       
    epsilon         %numerical diffusivity
    alpha  = 1.0    %exponential factor for numerical diffusivity
    Ceps   = 0.5    %pre-exponential factor for numerical diffusivity
    HR              %high ressolution interpolator
%     K      = 0      %Interface curvature
%     DIV             %Divergence operator enforcing Neumann boundary conditions
%     GRAD            %Gradient operator enforcing Neumann boundary conditions
%     sCF             %Scalar Cell-to-Face operator enforcing Neumann boundary conditions
%     vCF             %Vector Cell-to-Face operator enforcing Neumann boundary conditions
end
methods
    function f  = PF(M)
        %SUPERDIRTY copy constructor for M
        fns       = properties(M);
        for i=1:length(fns)
          f.M.(fns{i}) = M.(fns{i});
        end
        
        f.E       = 1/4*f.M.T.Dx;
        f.epsilon = full(f.Ceps*diag(f.M.T.Vc).^(1/2*f.alpha));

        %Enforce Neumann Boundary conditions for the LS Manifold
        f.M.T.sCF = f.M.T.deg\abs(f.M.T.Tc*f.M.T.PT)';
        f.M.T.vCF = f.M.T.N*kron(f.M.T.Id,f.M.T.deg\abs(f.M.T.Tc*f.M.T.PT)');
        f.M.T.D   = (f.M.T.PT-f.M.T.NF+f.M.T.DF)*f.M.T.Tu';
        f.M.GRAD  = f.M.T.Dx\f.M.T.D;
        f.M.DIV   = -f.M.T.Vc\f.M.GRAD'*f.M.T.Vs;

        f.HR      = FL(f.M,@SUPERBEE);    %high ressolution scheme
    end
    function r  = R(f,th,u)
        r = -f.CONV(u,th)*th + f.DIFF(u,th)*th;
    end
    function c  = CONV(m,u,th)
        U  = spdiags(u,0,m.M.T.ne,m.M.T.ne);
        c  = m.M.DIV*U*(m.M.T.sCF + m.HR.F(u,th));  %convection (FL)
%         c  = m.M.DIV*U*(m.M.T.sCF + m.HR.F(uf,th));  %convection (FL)
%         nf = m.normal(th);
%         Of = spdiags((1-m.M.T.sCF*th).*nf,0,m.M.T.ne,m.M.T.ne);
%         c  = c + m.M.DIV*Of*m.M.T.sCF;             %include recompression
    end
    function d = DIFF(m,u,th)
        R   = m.Recompression(th);
        U   = spdiags(u                     ,0,m.M.T.ne,m.M.T.ne);
        N   = spdiags(m.normal(th)           ,0,m.M.T.ne,m.M.T.ne);
        d   = m.M.DIV*U*(-R + m.E)*m.M.GRAD;   %Valle2019c       
    end
    function r = Recompression(m,th)
        r   = m.M.T.Dx*spdiags(m.M.T.sCF*((1-th).*th)           ,0,m.M.T.ne,m.M.T.ne);
%         r   = m.M.T.Dx*spdiags(m.M.T.sCF*(1-th).*(m.M.T.sCF*th) ,0,m.M.T.ne,m.M.T.ne);
    end
    function k = curvature(f,u,th)
        n = f.normal(th);
        N = spdiags(n,0,f.M.T.ne,f.M.T.ne);

%         S = f.M.T.sCF;                                  %Olsson2005
        
%         S = f.M.T.sCF - f.HR.F(u,th);                   %Valle2019
                
        R  = f.Recompression(th);
        S  = f.M.T.sCF - f.HR.F(u,th) - (R - f.E)*f.M.GRAD;    %Valle2019b
   
        k = -S*f.M.DIV*n;               
    end
    function n = normal(f,th)
%         d   = (f.M.T.sCF*th).*(f.M.T.sCF*(1-th));
%         d(abs(d)<1e-12) = 1;
%         n   = 0.5*(d.\(f.M.T.D*th));
        GT  = reshape(f.M.T.vFC*f.M.GRAD*th,f.M.T.nn,f.M.T.d);
        nc  = normr(GT);
        n   = f.M.T.vCF*nc(:);
    end
    function nc = normalc(f,th)
        GT  = reshape(f.M.T.vFC*f.M.GRAD*th,f.M.T.nn,f.M.T.d);
        nc  = normr(GT);
    end
    function t  = tangent(f,n)
        t   = [n(:,1),-n(:,2)];                 %WARNING: 2 dimensions only!!
    end
    function c  = circularity(f,th,r)
        ap= 2*pi*r;
        np= diag(f.M.T.Vc)'*sqrt(sum(reshape(f.M.GRADc*th,f.M.T.nn,f.M.T.d).^2,2));
        c = ap/np;
    end
    function [th,n,k] = init(f,shape,varargin)
        dx  = shape(f.M.T.xn,varargin{:});
        th  = 0.5*(tanh(dx./(2*f.epsilon))+1);
%         th = dx>0;
%         k   = f.K*f.interface(th);
%         k = 0.5*f.M.DIV*f.M.GRAD*dx;
        n = f.normal(th);
        u = zeros(f.M.T.ne,1);
        k = f.curvature(u,th);
    end
    function d  = distance(f,th)
        th(th<=0) = 1e-12;
        th(th>=1) = 1-1e-12;
        d = 2*f.epsilon.*(atanh((2*th-1)));   %SECURITY DOWNSCALING
    end
    function I = interface(p,theta)                 %interface faces
        I   = p.M.T.D*(double(theta>0.5));
    end
    function dx = film(f,x,h)
        f.K = 0;
        dx  = (x(:,1)-h);
    end
    function dx = bubble(f,x,xc,r)
        f.K = 1/r;
        dx  = arrayfun(@(c) -norm(x(c,:)-xc)+r, 1:size(x,1))';    
    end
    function dx = rectangle(f,x,h,w)
        dx = arrayfun(@(c) min(min(h/2-abs(x(c,1)),w/2-abs(x(c,2))),sqrt(norm([h/2,w/2]-abs(x(c,:)),2))),1:size(x,1))';
    end
    function dx = ellipse(f,x,a,b)
        d=@(th,x)(x-[a*cos(th),b*sin(th)]);
        for i=1:size(x,1)
            func  = @(th)norm(d(th,x(i,:)));
            th(i) = fminsearch(func,atan((x(i,2))/abs(x(i,1))));    %LESS OP.
        end
        dx = vecnorm(d(th',x),2,2).*(-1+2*double((x(:,1).^2/a^2+x(:,2).^2/b^2)<1));
    end
    function dx = rhodorea(f,x,r0,r1,w)
        r   = @(th)r0+r1*sin(w*th);
        d   = @(th,x)(x-[r(th).*cos(th),r(th).*sin(th)]);
        [t,rp] = cart2pol(x(:,1),x(:,2));
        for i=1:size(x,1)
            f     = @(th)norm(d(th,x(i,:)));
            th(i) = fminsearch(f,t(i));    %LESS OP.
        end
        dx  = vecnorm(d(th',x),2,2).*(-1+2*double(rp<r(t)));
    end
    function dx = sin(f,x,h,w)
        dx  = (x(:,1)-h*sin(w*x(:,2)/2*2*pi()));
    end
    function dx = jet(f,x,h,w)
        dx = (abs(x(:,1))-h*(1+0.2*sin(w*x(:,2))));
    end
    function [n,t] = geodesics(f,th)
        n   = f.normal(th);
        t   = f.tangent(n);
    end
    function [Dip,Din,Di,I] = DInterface(f,th)
        dc  = f.distance(th);
        I   = spdiags(f.interface(th),0,f.M.T.ne,f.M.T.ne);
%         Dip = spdiags((I*[(f.M.T.Tu')]>0)*dc,0,f.M.T.ne,f.M.T.ne);
%         Din = spdiags((I*[(f.M.T.Tu')]<0)*dc,0,f.M.T.ne,f.M.T.ne);
        [~,Pp,Pn] = f.phases(th);                               %TEST21/03/18
        Dip = spdiags(abs(f.M.T.Tu')*Pp*dc,0,f.M.T.ne,f.M.T.ne);   %TEST21/03/18
        Din = spdiags(abs(f.M.T.Tu')*Pn*dc,0,f.M.T.ne,f.M.T.ne);   %TEST21/03/18
%         Di  = speye(f.M.T.ne)-abs(I)+Dip-Din;
        Di  = Dip-Din;                                        %TEST21/03/18
    end
    function [P,Pp,Pn] = phases(f,th)
        Pp  = spdiags(double(th>0.5),0,f.M.T.nn,f.M.T.nn);
        Pn  = spdiags(double(th<0.5),0,f.M.T.nn,f.M.T.nn);
        P   = Pp - Pn;
    end
    function H = homothetic(f,th)               %Homothetic interpolator
        I   = spdiags(f.interface(th),0,f.M.T.ne,f.M.T.ne);
%         [Dip,Din,Di] = f.DInterface(th);
        [~,Pp,Pn]    = f.phases(th);
%         H   = -Di\(I*(Dip*[(f.M.T.Tc'),sparse(f.M.T.ne,f.M.T.ng)]*Pn+...
%                    Din*[(f.M.T.Tc'),sparse(f.M.T.ne,f.M.T.ng)]*Pp));
%         H   = Di\(I*(Dip*f.M.T.Tc'*Pn + Din*f.M.T.Tc'*Pp));        %TEST
%         H   = -Di\(Dip*abs(f.M.T.Tc')*Pn - Din*abs(f.M.T.Tc')*Pp);   %TEST
        H = (abs(f.M.T.D)*(Pp.*th + Pn.*th))./(abs(f.M.T.Tc)'*th);
    end
    function x = xh(f,th)                       %homothetic center
        x = abs(f.interface(th)).*f.homothetic(th)*f.M.T.xn;
    end
end
end
