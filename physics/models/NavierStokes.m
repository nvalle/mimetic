classdef NavierStokes
    %NavierStokes Implementation of Navier-Stokes equations in a
    %computational setting
    %   NavierStokes impose discrete mass and momentum conservation over a
    %   finite-dimensional manifold. The equations are formulated over a
    %   Discrete Vector Calculus (DVC) instance.
    %   In addition to a DVC, NavierStokes includes a constituent equation,
    %   which contains the physical properties (i.e., the metric relations)
    %   that hold for the given flow system.
    
    properties
        M           %discrete manifold. contains differential operators.
        C           %constitutive equation. contains physical properties.
    end
    
    methods
        function this = NavierStokes(M,C)
            %NavierStokes Constructs an instance of NavierStokes
            %   M : DVC instance. Differential Manifold.
            %   C : Constituent instance.
            this.M = M;
            this.C = C;
        end        
        function dmdt = Rp(this,u,p)
            %Rp Computes time derivative of the momentum equation
            %   u : velocity field
            %   P : pressure field
            dmdt = -this.CONV(u) + this.DIFF(u);
        end
        function c = CONV(this,u)
            %CONV Computes convective term of the momentum equation
            %   u : velocity field
            c = this.M.A(u,u,@M.SP);
        end
        function d = DIFF(this,u)
            %DIFF Computes diffusive term of the momentum equation
            %   u : velocity field
            g = this.M.G*u;
            f = this.C.mu*g;
            d = this.M.D*f;
        end
        function [u,p] = init(this)
            u = this.M.init(1,2);
            p = this.M.init(1,1);
        end
    end
end

