classdef EPLS < THLS
properties
end
% methods (Abstract)
%     r = RSH(f,th,n);    %Resharpening
% end
methods
    function f  = EPLS(M)
        f@THLS(M);
    end
    function k = curvature(f,uf,th)
        n = f.normal(th);
        
%         S = f.M.T.sCF;                              %Olsson2005&2007
        
        S = f.M.T.sCF - f.HR.F(uf,th);                %Valle2019a
        
        %----------TEST
%         uf(abs(uf)<1e-16) = 1;
%         U  = spdiags(uf                     ,0,f.M.T.ne,f.M.T.ne);
%         N  = spdiags(f.normal(th)         ,0,f.M.T.ne,f.M.T.ne);
%         R  = spdiags(f.M.T.Dx*f.M.T.sCF*((1-th).*th) , 0,f.M.T.ne,f.M.T.ne);
%         E  = spdiags(f.M.T.sCF*f.epsilon    ,0,f.M.T.ne,f.M.T.ne);
%    
%         S  = S - (R - E)*f.M.GRAD;    %Valle2019b
   
        k = -S*f.M.DIV*n;               
    end
end    
end
