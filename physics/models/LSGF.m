classdef LS < handle
properties
    M               %Manifold over which the equation is formulated
    epsilon         %numerical diffusivity
    alpha  = 0.9    %exponential factor for numerical diffusivity
    Ceps   = 0.5    %pre-exponential factor for numerical diffusivity
end
methods
    function f = LS(M)
        f.M       = M;
        f.epsilon = max(f.Ceps*diag((0.5*(sqrt(f.M.T.Vc).^f.alpha))));
    end
    function r = R(f,u,theta)
        r = 0;
    end
    function theta = init(f,xi)
        dx = (f.M.T.xc(:,1) - xi);
        theta = 0.5*(tanh(dx/2/f.epsilon)+1);
    end

end
end