classdef GF < handle
    %property manager suited for a Ghost Fluid implementation
properties
    T               %topological space
    M               %manifold
    ieq             %interface equation
    rho0            %reference density
    rhor            %density ratio
    mu0             %reference viscosity
    mur             %viscosity ratio
    st              %surface tension
end
methods
    function p = GF(M,ieq,rh0,mu0,rhr,mur)
        p.M    = M;
        p.T    = M.T;
        p.ieq  = ieq;
        p.rho0 = rh0;
        p.rhor = rhr;
        p.mu0  = mu0;
        p.mur  = mur;
        p.st   = 1/100;
    end
    function rh = rhoc(p,theta)
        %whole domain
        %linear
%         rh = p.rho0*(theta + p.rhor*(1-theta));
        %interface only
        I  = p.interfaceC(theta);                              %interface cells
        %switched
        rh = p.rho0*((theta<0.5)+p.rhor*(theta>=0.5));
        %linear - Interface
%         rh = rh + I.*(p.rho0*(theta + p.rhor*(1-theta))-rh);
        %linear - Full domain
%         rh = p.rho0*(theta + p.rhor*(1-theta));
        %harmonic - Interface
%         rh = rh + I.*(p.rho0^2*p.rhor./((p.rho0*((1-theta))+(p.rhor*(theta))))-rh);
        %logarithmic
%         rh = rh + I.*(p.rho0*(theta-p.rhor*(1-theta))./log((1-theta)./(p.rhor*(theta)))-rh);
        rh = spdiags(rh,0,p.M.T.nn,p.M.T.nn);
    end
    function rh = rhof(p,rhoc,Di,Dp,I)
%         rh = spdiags(p.T.sCF*rhoc,0,p.M.T.ne,p.M.T.ne);
        rh = spdiags(Di\p.T.Tb'*Dp*diag(rhoc),0,p.M.T.ne,p.M.T.ne);
    end
    function rh = rhoh(p,phi,rhc)
        rhh  = p.T.HarmonicMean(diag(rhc),phi);
        rh   = spdiags(rhh,0,p.M.T.ne,p.M.T.ne); 
    end
    
    function mu = muc(p,theta)
        %CELL-based
        %switched   @cells
        muc = p.mu0*((theta<0.5) + p.mur*(theta>=0.5));
        %linear     @cells
%         muc = p.mu0*(theta + p.mur*(1-theta));
        %Cell-to-Face extrapolation
%         mu  = p.M.T.sCF*muc;
        mu  = spdiags(muc,0,p.M.T.nn,p.M.T.nn);
    end
    function mu = mucLINEAR(p,theta)
        %CELL-based
        %linear     @cells
        muc = p.mu0*(theta + p.mur*(1-theta));
        %Cell-to-Face extrapolation
%         mu  = p.M.T.sCF*muc;
        mu  = spdiags(muc,0,p.M.T.nn,p.M.T.nn);
    end
    function mu = muf(p,phi,muc)
        mu = phi.*(p.T.pTc'*diag(muc)) + (1-phi).*(p.T.nTc'*diag(muc));
    end
    function mu = muh(p,phi,muc)
        %harmonic   @faces  Liu2000
%         muh  = p.T.HarmonicMean(diag(muc),p.ieq.distance(phi));
%         muc = p.mu0*((phi>=0.5) + p.mur*(phi<0.5));     %mu @ cells
        mup = p.T.pTc'*diag(muc);                                 %mu @ cells +
        mun = p.T.nTc'*diag(muc);                                 %mu @ cells -
%         muh  = (mup.*mun)./((mup).*(1-thf)+(mun).*thf);
        %n-power face averaged
%         n   = 2;
%         mu  = (mup.^(n/2).*mun.^(n/2)).^(1/n);
        muh = (p.ieq.homothetic(phi)*diag(muc)).\mup.*mun...
             + p.T.BF*p.T.Tb'*diag(muc);        %Explicitly add boundary faces
        mu  = spdiags(muh,0,p.M.T.ne,p.M.T.ne); 
    end


    function phi = SIL(p,theta)                     %Subcell Interface Location
%         num = p.M.T.nTc'*theta;
%         den = spdiags(abs(p.M.T.D)'*theta,0,p.M.T.ne,p.M.T.ne)+speye(p.M.T.ne)+abs(p.interface(theta));
%         phi = den\num;
        phi = (p.M.T.nTc'*theta)./(abs(p.M.T.D)'*theta+1-abs(p.ieq.interface(theta)));
    end
    function I = interfaceC(p,theta)                %interface cells
        I   = zeros(p.M.T.nn,1);
        I   = p.M.DIV*p.M.GRAD*abs(theta-0.5)>0;
    end
    function [rho,mu] = init(p,theta)
        rho = p.rhoc(theta);
        mu  = p.muc(theta);
    end
end
end