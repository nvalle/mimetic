classdef Sussman1994 < handle
properties
    M               %Manifold over which the equation is formulated
    epsilon         %numerical diffusivity
    alpha  = 1.0    %exponential factor for numerical diffusivity
    Ceps   = 0.5    %pre-exponential factor for numerical diffusivity
    FL              %flux limiter
    K      = 0      %Interface curvature
end
methods
    function f  = Sussman1994(M)
        f.M       = M;
        f.epsilon = full(f.Ceps*diag(f.M.T.Vc).^(1/2*f.alpha));
        f.FL      = FL(M,@SUPERBEE);
    end
    function r  = R(f,th,u)
        r = f.CONV(u,th);
    end
    function c  = CONV(m,u,th)
        %NOTE: instead of reshape to vectorize u, we use u(:).
        uf = spdiags(m.M.T.vCF*u(:),0,m.M.T.ne,m.M.T.ne);   %face flow
        c  = -m.M.DIV*uf*(m.M.T.sCF + m.FL.F(diag(uf),th))*th;  %convection (FL)
    end
    function [th,k] = init(f,shape,varargin)
        dx  = shape(f.M.T.xn,varargin{:});
        th  = 0.5*(tanh(dx./(2*f.epsilon))+1);
%         th = dx>0;
%         k   = f.K*f.interface(th);
%         k = 0.5*f.M.DIV*f.M.GRAD*dx;
        k = f.curvature(th);
    end
    function d  = distance(f,th)
        th(th<=0) = 1e-12;
        th(th>=1) = 1-1e-12;
        d = 2*f.epsilon.*(atanh((2*th-1)));   %SECURITY DOWNSCALING
    end
    function I = interface(p,theta)                 %interface faces
        I   = p.M.T.D*(double(theta>0.5));
    end
    function k = curvature(f,th)
%         dx = f.distance(th);
%         k = -f.interface(th).*f.homothetic(th)*f.M.DIV*(f.M.T.Dx\(f.M.T.D*dx));
        n = f.normal(th);
        kc = f.M.DIV*f.M.T.vCF*n(:);
        k = f.interface(th).*f.M.T.sCF*kc;
%         kp = f.M.T.pTc'*kc;
%         kn = f.M.T.nTc'*kc;
%         k = (f.homothetic(th)*kc).\(kp.*kn) + f.M.T.BF*f.M.T.Tb'*kc;
%         k = f.interface(th).*(f.homothetic(th)*f.M.DIV*f.M.T.vCF*n(:));
%         k = 1/0.3*f.interface(th);
    end
    function dx = film(f,x,h)
        f.K = 0;
        dx  = (x(:,1)-h);
    end
    function dx = bubble(f,x,xc,r)
        f.K = 1/r;
        dx  = arrayfun(@(c) -norm(x(c,:)-xc)+r, 1:size(x,1))';    
    end
    function dx = rectangle(f,x,h,w)
        dx = arrayfun(@(c) min(min(h/2-abs(x(c,1)),w/2-abs(x(c,2))),sqrt(norm([h/2,w/2]-abs(x(c,:)),2))),1:size(x,1))';
    end
    function dx = ellipse(f,x,a,b)
        d=@(th,x)(x-[a*cos(th),b*sin(th)]);
        for i=1:size(x,1)
            func  = @(th)norm(d(th,x(i,:)));
            th(i) = fminsearch(func,atan((x(i,2))/abs(x(i,1))));    %LESS OP.
        end
        dx = vecnorm(d(th',x),2,2).*(-1+2*double((x(:,1).^2/a^2+x(:,2).^2/b^2)<1));
    end
    function dx = rhodorea(f,x,r0,r1,w)
        r   = @(th)r0+r1*sin(w*th);
        d   = @(th,x)(x-[r(th).*cos(th),r(th).*sin(th)]);
        [t,rp] = cart2pol(x(:,1),x(:,2));
        for i=1:size(x,1)
            f     = @(th)norm(d(th,x(i,:)));
            th(i) = fminsearch(f,t(i));    %LESS OP.
        end
        dx  = vecnorm(d(th',x),2,2).*(-1+2*double(rp<r(t)));
    end
    function dx = sin(f,x,h,w)
        dx  = (x(:,1)-h*sin(w*x(:,2)));
    end
    function c  = circularity(f,th,r)
        ap= 2*pi*r;
        np= diag(f.M.T.Vc)'*sqrt(sum(reshape(f.M.GRADc*th,f.M.T.nn,f.M.T.d).^2,2));
        c = ap/np;
    end
    function n  = normal(f,th)
%         d   = f.distance(th);
        GT  = reshape(f.M.GRADc*th,f.M.T.nn,f.M.T.d);
        n   = normr(GT);
    end
    function t  = tangent(f,n)
        t   = [n(:,1),-n(:,2)];                 %WARNING: 2 dimensions only!!
    end
    function [n,t] = geodesics(f,th)
        n   = f.normal(th);
        t   = f.tangent(n);
    end
    function [Dip,Din,Di,I] = DInterface(f,th)
        dc  = f.distance(th);
        I   = spdiags(f.interface(th),0,f.M.T.ne,f.M.T.ne);
%         Dip = spdiags((I*[(f.M.T.Tu')]>0)*dc,0,f.M.T.ne,f.M.T.ne);
%         Din = spdiags((I*[(f.M.T.Tu')]<0)*dc,0,f.M.T.ne,f.M.T.ne);
        [~,Pp,Pn] = f.phases(th);                               %TEST21/03/18
        Dip = spdiags(abs(f.M.T.Tu')*Pp*dc,0,f.M.T.ne,f.M.T.ne);   %TEST21/03/18
        Din = spdiags(abs(f.M.T.Tu')*Pn*dc,0,f.M.T.ne,f.M.T.ne);   %TEST21/03/18
%         Di  = speye(f.M.T.ne)-abs(I)+Dip-Din;
        Di  = Dip-Din;                                        %TEST21/03/18
    end
    function [P,Pp,Pn] = phases(f,th)
        Pp  = spdiags(double(th>0.5),0,f.M.T.nn,f.M.T.nn);
        Pn  = spdiags(double(th<0.5),0,f.M.T.nn,f.M.T.nn);
        P   = Pp - Pn;
    end
    function H = homothetic(f,th)               %Homothetic interpolator
        I   = spdiags(f.interface(th),0,f.M.T.ne,f.M.T.ne);
%         [Dip,Din,Di] = f.DInterface(th);
        [~,Pp,Pn]    = f.phases(th);
%         H   = -Di\(I*(Dip*[(f.M.T.Tc'),sparse(f.M.T.ne,f.M.T.ng)]*Pn+...
%                    Din*[(f.M.T.Tc'),sparse(f.M.T.ne,f.M.T.ng)]*Pp));
%         H   = Di\(I*(Dip*f.M.T.Tc'*Pn + Din*f.M.T.Tc'*Pp));        %TEST
%         H   = -Di\(Dip*abs(f.M.T.Tc')*Pn - Din*abs(f.M.T.Tc')*Pp);   %TEST
        H = (abs(f.M.T.D)*(Pp.*th + Pn.*th))./(abs(f.M.T.Tc)'*th);
    end
    function x = xh(f,th)                       %homothetic center
        x = abs(f.interface(th)).*f.homothetic(th)*f.M.T.xn;
    end
end
end
