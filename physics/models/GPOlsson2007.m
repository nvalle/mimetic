classdef GPOlsson2007 < EPLS
properties
end
methods
    function f  = GPOlsson2007(M)
        f@EPLS(M);
    end
    function r  = RSH(f,th,n,u)         %Resharpening
        r = -f.RCOMP(n,th,u)*th+f.DIFF(n,th,u)*th;
    end
    function d  = DIFF(m,n,th,u)        %Diffusion
%         GT  = reshape(m.M.GRADc*th,m.M.T.nn,m.M.T.d);
%         PG  = spdiags(m.M.T.DOT(GT,n(:)),0,m.M.T.nn,m.M.T.nn)*n;
%         d   = m.M.DIV*m.diff*m.M.T.vCF*PG(:);     %Olsson, 2007
%         d   = 0.5*m.M.DIV*(n.*(m.M.T.D*th));      %Olsson, 2007
                
%         d   = m.M.DIV*m.epsilon*m.M.GRAD;       %Olsson, 2005

        N   = spdiags(f.M.T.N*f.M.T.N'*n,0,m.M.T.ne,m.M.T.ne);
        d   = m.M.DIV*m.epsilon*N*m.M.GRAD;     %Olsson, 2007        

%         U   = spdiags(u,0,m.M.T.ne,m.M.T.ne);
%         d   = 0.5*m.M.DIV*m.M.T.D;           %Valle, 2019
    end
    function c  = RCOMP(m,n,th,u)       %Recompression
%         We will compute a new value of n, as the one provided as an
%         argument here is already interpolated at the faces, whereas
%         Olsson2005 preffers to directly interpolate the product
%         n*(1-th)*th.
        GT  = reshape(m.M.T.vFC*m.M.GRAD*th,m.M.T.nn,m.M.T.d);
        nc  = normr(GT);
        i   = [0:m.M.T.nn:m.M.T.nn*(m.M.T.d-1)];        %index
        N   = spdiags(nc,i,m.M.T.nn,m.M.T.nn*m.M.T.d);  %normal
        R   = spdiags(1-th,0,m.M.T.nn,m.M.T.nn);        %recompression
        c   = m.M.DIV*m.M.T.vCF*N'*R;                   %Olsson2005&2007

%         N   = spdiags(n                     ,0,m.M.T.ne,m.M.T.ne);
%         R   = spdiags(m.M.T.sCF*(1-th)      ,0,m.M.T.ne,m.M.T.ne);
%         c   = m.M.DIV*N*R*m.M.T.sCF;    %Valle2019a
        
%         N   = spdiags(n                     ,0,m.M.T.ne,m.M.T.ne);
%         R   = spdiags((1-th)                ,0,m.M.T.nn,m.M.T.nn);
%         c   = m.M.DIV*N*m.M.T.sCF*R;    %Valle2019b

%         U   = spdiags(u,0,m.M.T.ne,m.M.T.ne);
%         R   = spdiags((m.M.T.sCF*((1-th).*th)),0,m.M.T.ne,m.M.T.ne);
%         c   = m.M.DIV*R*m.M.T.Dx*m.M.GRAD;   %Valle2019c
    end
end
end
