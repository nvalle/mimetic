classdef (Abstract) CLS < THLS
properties
end
methods (Abstract)
    r = RSH(f,th,n);    %Resharpening
end
methods
    function f  = CLS(M)
        f@THLS(M);
    end
    function k = curvature(f,uf,th)
        n = f.normal(th);
        
        S = f.M.T.sCF;                              %Olsson2005&2007
        
%         S = f.M.T.sCF - f.HR.F(uf,th);                %Valle2019a
   
        k = -S*f.M.DIV*n;               
    end

end    
end
