classdef (Abstract) LS < handle
properties
    M               %Manifold over which the equation is formulated
    DIV             %Divergence operator enforcing Neumann boundary conditions
    GRAD            %Gradient operator enforcing Neumann boundary conditions
    sCF             %Scalar Cell-to-Face operator enforcing Neumann boundary conditions
    vCF             %Vector Cell-to-Face operator enforcing Neumann boundary conditions
end
methods (Abstract)
    r  = ADV(f,th,u)
%     c  = RSH(m,u,th)
end
methods
    function f  = LS(M)
        %SUPERDIRTY copy constructor for M
%         fns       = properties(M);
%         for i=1:length(fns)
%           f.M.(fns{i}) = M.(fns{i});
%         end
        f.M       = copy(M);
        f.M.T     = copy(M.T);
%         f.M.T.sCF = M.T.(sCF);
%         f.M.T.vCF = M.T.(vCF);
%         f.M.T.D   = M.(T.D);
%         f.M.GRAD  = M.(GRAD);
%         f.M.DIV   = M.(DIV);
        %Enforce Neumann Boundary conditions for the LS Manifold
        f.M.T.sCF = f.M.T.deg\abs(f.M.T.Tc*f.M.T.PT)';
        f.M.T.vCF = f.M.T.N*kron(f.M.T.Id,f.M.T.deg\abs(f.M.T.Tc*f.M.T.PT)');
        f.M.T.D   = (f.M.T.PT-f.M.T.NF+f.M.T.DF)*f.M.T.Tu';
        f.M.GRAD  = f.M.T.Dx\f.M.T.D;
        f.M.DIV   = -f.M.T.Vc\f.M.GRAD'*f.M.T.Vs;
    end
    function c  = circularity(f,th,r)
        ap= 2*pi*r;
        np= diag(f.M.T.Vc)'*sqrt(sum(reshape(f.M.GRADc*th,f.M.T.nn,f.M.T.d).^2,2));
        c = ap/np;
    end
    function d  = distance(f,th)
        th(th<=0) = 1e-12;
        th(th>=1) = 1-1e-12;
        d = 2*f.epsilon.*(atanh((2*th-1)));   %SECURITY DOWNSCALING
    end
    function I = interface(p,theta)                 %interface faces
        I   = p.M.T.D*(double(theta>0.5));
    end
    function dx = film(f,x,h)
        f.K = 0;
        dx  = (x(:,1)-h);
    end
    function dx = bubble(f,x,xc,r)
        dx  = arrayfun(@(c) -norm(x(c,:)-xc)+r, 1:size(x,1))';    
    end
    function dx = rectangle(f,x,h,w)
        dx = arrayfun(@(c) min(min(h/2-abs(x(c,1)),w/2-abs(x(c,2))),sqrt(norm([h/2,w/2]-abs(x(c,:)),2))),1:size(x,1))';
    end
    function dx = ellipse(f,x,a,b)
        d=@(th,x)(x-[a*cos(th),b*sin(th)]);
        for i=1:size(x,1)
            func  = @(th)norm(d(th,x(i,:)));
            th(i) = fminsearch(func,atan((x(i,2))/abs(x(i,1))));    %LESS OP.
        end
        dx = vecnorm(d(th',x),2,2).*(-1+2*double((x(:,1).^2/a^2+x(:,2).^2/b^2)<1));
    end
    function dx = rhodorea(f,x,r0,r1,w)
        r   = @(th)r0+r1*sin(w*th);
        d   = @(th,x)(x-[r(th).*cos(th),r(th).*sin(th)]);
        [t,rp] = cart2pol(x(:,1),x(:,2));
        for i=1:size(x,1)
            f     = @(th)norm(d(th,x(i,:)));
            th(i) = fminsearch(f,t(i));    %LESS OP.
        end
        dx  = vecnorm(d(th',x),2,2).*(-1+2*double(rp<r(t)));
    end
    function dx = sin(f,x,h,w)
        dx  = (x(:,1)-h*sin(w*x(:,2)/2*2*pi()));
    end
    function dx = jet(f,x,h,w)
        dx = (abs(x(:,1))-h*(1+0.2*sin(w*x(:,2))));
    end
    function [n,t] = geodesics(f,th)
        n   = f.normal(th);
        t   = f.tangent(n);
    end
    function [Dip,Din,Di,I] = DInterface(f,th)
        dc  = f.distance(th);
        I   = spdiags(f.interface(th),0,f.M.T.ne,f.M.T.ne);
%         Dip = spdiags((I*[(f.M.T.Tu')]>0)*dc,0,f.M.T.ne,f.M.T.ne);
%         Din = spdiags((I*[(f.M.T.Tu')]<0)*dc,0,f.M.T.ne,f.M.T.ne);
        [~,Pp,Pn] = f.phases(th);                               %TEST21/03/18
        Dip = spdiags(abs(f.M.T.Tu')*Pp*dc,0,f.M.T.ne,f.M.T.ne);   %TEST21/03/18
        Din = spdiags(abs(f.M.T.Tu')*Pn*dc,0,f.M.T.ne,f.M.T.ne);   %TEST21/03/18
%         Di  = speye(f.M.T.ne)-abs(I)+Dip-Din;
        Di  = Dip-Din;                                        %TEST21/03/18
    end
    function [P,Pp,Pn] = phases(f,th)
        Pp  = spdiags(double(th>0.5),0,f.M.T.nn,f.M.T.nn);
        Pn  = spdiags(double(th<0.5),0,f.M.T.nn,f.M.T.nn);
        P   = Pp - Pn;
    end
    function H = homothetic(f,th)               %Homothetic interpolator
        I   = spdiags(f.interface(th),0,f.M.T.ne,f.M.T.ne);
%         [Dip,Din,Di] = f.DInterface(th);
        [~,Pp,Pn]    = f.phases(th);
%         H   = -Di\(I*(Dip*[(f.M.T.Tc'),sparse(f.M.T.ne,f.M.T.ng)]*Pn+...
%                    Din*[(f.M.T.Tc'),sparse(f.M.T.ne,f.M.T.ng)]*Pp));
%         H   = Di\(I*(Dip*f.M.T.Tc'*Pn + Din*f.M.T.Tc'*Pp));        %TEST
%         H   = -Di\(Dip*abs(f.M.T.Tc')*Pn - Din*abs(f.M.T.Tc')*Pp);   %TEST
        H = (abs(f.M.T.D)*(Pp.*th + Pn.*th))./(abs(f.M.T.Tc)'*th);
    end
    function x = xh(f,th)                       %homothetic center
        x = abs(f.interface(th)).*f.homothetic(th)*f.M.T.xn;
    end
end
end
