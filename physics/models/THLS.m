classdef (Abstract) THLS < LS
properties
    epsilon         %numerical diffusivity
    alpha  = 1.0    %exponential factor for numerical diffusivity
    Ceps   = 0.5    %pre-exponential factor for numerical diffusivity
    HR              %high ressolution interpolatorend
end
methods (Abstract)
    k = curvature(f,th,n);    %Resharpening
end
methods
    function f  = THLS(M)
        f@LS(M);
        f.epsilon   = 1/2*f.M.T.Dx;
        f.HR        = FL(f.M,@SUPERBEE);
    end
    function [th,n,k] = init(f,shape,varargin)
        e   = sqrt(diag(f.M.T.Vc));
        dx  = shape(f.M.T.xn,varargin{:});
        th  = 0.5*(tanh(dx./(2*e))+1);
%         th = dx>0;
%         k   = f.K*f.interface(th);
%         k = 0.5*f.M.DIV*f.M.GRAD*dx;
        n = f.normal(th);
        u = zeros(f.M.T.ne,1);
        k = f.curvature(u,th);
    end
    function r  = ADV(f,th,u)
        U  = spdiags(u,0,f.M.T.ne,f.M.T.ne);
        r  = -f.M.DIV*U*(f.M.T.sCF + f.HR.F(u,th))*th;  %convection (FL)
    end
    function n = normal(f,th)
        GT  = reshape(f.M.T.vFC*f.M.GRAD*th,f.M.T.nn,f.M.T.d);
        nc  = normr(GT);
        n   = f.M.T.vCF*nc(:);
    end
end    
end
