classdef fluid < constitutive
    %fluid is a pure base class that contains the basic constitutive
    %relations which are typical for a fluid.
    %   This function is supposed to be specialized by specialized
    %   constitutive equations.
    
    properties
        
    end
    
    methods
        function this = fluid(M)
            %fluid Construct an instance of fluid
            %   Detailed explanation goes here
           this@constitutive(M);
        end
    end
end

