classdef Newtonian < fluid
    %Newtonian contains an incompressible Newtonian fluid.
    %   An incompressible Newtonian fluid is determined directly by the
    %   non-dimensional Reynolds number.
    %   ToDo: generalize such a relation to split between Newtonian and
    %   Incompressible fluid.
    
    properties
        Re          %Reynolds
        rho         %density
        mu          %viscosity (dynamic)
    end
    
    methods
        function this = Newtonian(M,Re)
            %Newtonian Construct an instance of Newtonian fluid
            %   Because of the non-dimensional nature of the formulation,
            %   we will set density to unity and only define viscosity as a
            %   constant.
            this@fluid(M);
            this.Re     = Re;
            this.rho    = 1;
            this.mu     = 1/this.Re;
        end
    end
end

