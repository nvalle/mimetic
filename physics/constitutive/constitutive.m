classdef constitutive
    %constitutive is a pure base class aimed at containing consititutive
    %relations.
    %   This class is not intended to contain functional information other
    %   that generic information, for instance, the manifold on top of
    %   which it supposed to metric relations. Note vertical (i.e., metric)
    %   relations in a DoubleComplex are supposed to be not only geometric,
    %   but potentially also physical, i.e., measured.
    
    properties
        M       %Manifold
    end
    
    methods
        function this = constitutive(M)
            %constitutive Construct an instance of constitutive
            %   M : DoubleComplex object which constitutes the domain
            this.M = M;
        end
    end
end

