classdef (Abstract) algebraic < solver
%Solves Ax = b equations. Base class.
properties
%     A               %coefficients matrix
%     b               %independent term
end
methods (Abstract)
    x = solve(s,b);
end
% methods
%     function a = algebraic(A)
%         a.A = A;
%     end
% end
end

