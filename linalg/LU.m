classdef LU < algebraic
%Lower-Upper decompostion solver
properties
    L               %Lower decomposition matrix
    U               %Upper decomposition matrix
end
methods
    function s = LU(A)
        s@algebraic(A);
        [s.L,s.U] = lu(s.A);
    end
    function x = solve(s,b)
        x = s.U\(s.L\b);
    end
end
end

