classdef solver < handle
    %SOLVER base class. TO BE IMPLEMENTED 
properties
    s    = 0        %present step
    smax = 1        %max number of steps
    rmin = 1e-16    %convergence value
    rmax = 1e+16    %divergence value
    r    = 1.1e-9   %residual
end

methods
end
end

