classdef PCG_ICHOL < PCG
%Cholesky decompostion solver
properties
end
methods
    function s = PCG_ICHOL(A)
        alpha       = max(sum(abs(A),2)./diag(A))-2;
        M           = ichol(A, struct('type','ict','droptol',1e-3,'diagcomp',alpha));
        s@PCG(A,M);     
    end
    function x = solve(s,b)
        disp('PCG_ICHOL-solve')
        s.b = b;
        [x,~,r,iter] = pcg(s.A,s.b,s.tolerance,s.itmax,s.P,s.P');
%         [x,~,r,iter] = pcg(s.A,s.b,s.tolerance,s.itmax,s.P);
        s.r = r;
        s.s = iter;
    end
end
end

