classdef PFD < algebraic
%Periodic Fourier Decomposition solver.
%We will assume that data periodic in the last coordinate (in 2D -> y)
properties
    np          %size of periodic direction
    nt          %size of transversal direction
    St          %transverse solvers
end
methods
    function s = PFD(A,B)
        s.nt = size(B,1);
        s.np = size(A,1)/s.nt;
        %obtain periodic matrix
        index   = [1:s.nt:s.np*s.nt];
        Ap      = [A(index,index)];
        %normalize for the first area element
        Ap      = B(1,1)\(Ap - spdiags(sum(Ap,2),0,s.np,s.np));
        %obtain transverse submatrices
        for i = [1:s.np]
            index   = [(i-1)*s.nt+1:i*s.nt];
            At      = [A(index,index)];
            At      = At - spdiags(sum(At,2),0,s.nt,s.nt);
            %include eigenvalue at the diagonal
            ai      = (2*pi()*(i-1))/s.np;
            evali   = Ap(1,1);
            for r = [1:(s.np/2-1)]
                evali = evali + 2*Ap(1,1+r)*cos(r*ai);
            end
            At    = At + evali*B;
            %disturb first element of first matrix
            if i == 1
                At(1,1) = At(1,1)*1.1;
            end
            %generate transversal solver
            s.St{i} = CHOL(At);
        end
     end
    function x = solve(s,b)
        x  = [];
        bf = fft(reshape(b,s.nt,s.np).').';
        for i = [1:s.np]
            xf  = s.St{i}.solve(bf(:,i));
            x   = [x,xf];
        end
        x = ifft(x.').';
        x = real(x(:));
    end
end
end

