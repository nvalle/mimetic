classdef CHOL < algebraic
%Cholesky decompostion solver
properties
    C               %Upper decomposition matrix
end
methods
    function s = CHOL(A)
        [s.C] = chol(A);
     end
    function x = solve(s,b)
        x = s.C\(s.C'\b);
    end
end
end

