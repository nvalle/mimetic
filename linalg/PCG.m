classdef PCG < algebraic
%Cholesky decompostion solver
properties
    tolerance
    itmax
    P
end
methods
%     function s = PCG(A)
%         s@algebraic(A);
%         s.tolerance = 1e-5;
%         s.itmax     = 10000;
% %         s.P         = ichol(A,struct('michol','on')); %incomplete Cholesky factorization
%         alpha       = max(sum(abs(A),2)./diag(A))-2;
%         s.P         = ichol(A, struct('type','ict','droptol',1e-3,'diagcomp',alpha));
%     end
    function s = PCG(A,M)
        s@algebraic(A);
        s.tolerance = 1e-5;
        s.itmax     = 1000;
        alpha       = max(sum(abs(A),2)./diag(A))-2;
        s.P         = M;
    end
    function x = solve(s,b)
%         disp('PCG-solve');
        s.b = b;
%         [x,~,r,iter] = pcg(s.A,s.b,s.tolerance,s.itmax,s.P,s.P');
        [x,~,r,iter] = pcg(s.A,s.b,s.tolerance,s.itmax,s.P);
        s.r = r;
        s.s = iter;
    end
end
end

